/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import shop.*;

/**
 *
 * @author Luboš Kos
 */
public class StorageTest {
    
    public StorageTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testRemoveItem() throws NoItemInStorage{
        //setup
        ItemImpl item1 = new ItemImpl(1, "apples", 25, "fruit");
        ItemImpl item2 = new ItemImpl(2, "oranges", 55, "fruit");
        ItemImpl item3 = new ItemImpl(3, "lemons", 75, "fruit");
        
        Storage storage = new Storage();
        
        storage.insertItems(item1, 1);
        storage.insertItems(item2, 1);
        storage.insertItems(item3, 1);
        
        //act
        
        storage.removeItems(item1, 1);
        storage.removeItems(item2, 1);
        
        //assert
        assertEquals(0, storage.getItemCount(item1));
        assertEquals(0, storage.getItemCount(2));
    }
    
    @Test
    public void testNoItemToRemove() throws NoItemInStorage{
        //setup
        ItemImpl item1 = new ItemImpl(1, "apples", 25, "fruit");   
        Storage storage = new Storage();
        
        storage.insertItems(item1, 5);
        
        //act
        storage.removeItems(item1, storage.getItemCount(item1));
        //storage.removeItems(item1, 1);
        
        //assert
        fail("You tried to remove an item which is not in storage.");  
    }
    
    @Test
    public void testTryToRemoveUnInsertedItem() throws NoItemInStorage{
        //setup        
        ItemImpl item1 = new ItemImpl(1, "apples", 25, "fruit");
        
        Storage storage = new Storage();
        
        //act
        storage.removeItems(item1, 1);
        
        //asserts
        fail("You tried to remove uninserted item.");
    }
    
}
